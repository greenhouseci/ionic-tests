var jasmineReporters = require('jasmine-reporters');

var onPrepare = function() {};
if (process.env.GREENHOUSE) {
  var junitReporter = new jasmineReporters.JUnitXmlReporter({
    savePath: process.env.GREENHOUSE_XUNIT_RESULTS_DIR,
    consolidateAll: false,
  });

  onPrepare = function() {
    jasmine.getEnv().addReporter(junitReporter);
  };
}

exports.config = {
  framework: 'jasmine2',
  specs: ['test/protractor/**/*test.js'],
  onPrepare: onPrepare,
};
